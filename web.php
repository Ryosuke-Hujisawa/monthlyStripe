<?php


//======================================================================
// ポストでトークンを受け取って、ストライプの実装を開始する
//======================================================================
$token_from_js = $_POST['request'];
$from_email = $_POST['email_req'];
$from_name = $_POST['name_req'];
$from_tel = $_POST['tel_req'];


require_once 'vendor/autoload.php';
\Stripe\Stripe::setApiKey("");

//======================================================================
// トークンの情報を元にカスタマー情報を作る
//======================================================================
$customer =  \Stripe\Customer::create(
  array(
  "description" => "メールアドレス$from_email 名前$from_name 電話番号$from_tel",
  "source" => "$token_from_js",
  )
);

//======================================================================
// カスタマー情報を元に月額支払いのサブスクリプションを作るって実行する
//======================================================================
$subscription =  \Stripe\Subscription::create(
  array(
  "customer" => "$customer->id",
  "items" => array( array("plan" => "1733466696949178",), )
  )
);

//======================================================================
// 実行結果をechoで確認する
//======================================================================
// echo $subscription->id;
// echo $customer -> description;

// subscription = Stripe::Subscription.retrieve("sub_xxxx")
// Subscription.delete

// $sub = \Stripe\Subscription::retrieve("sub_CY38MZuhjsTtRc");
// $sub->cancel();

// echo $sub;


?>
