var stripe = Stripe('pk_test_sVKjjn36CXPKudxFv16zdSxM');
var elements = stripe.elements();
/*
 *
 *
 *
 *
 make card info
 *
 *
 *
 *
 */
var card = elements.create('card', {
  style: {
    base: {
      iconColor: '#666EE8',
      color: '#31325F',
      lineHeight: '40px',
      fontWeight: 300,
      fontFamily: 'Helvetica Neue',
      fontSize: '15px',
      '::placeholder': {
        color: '#CFD7E0',
      },
    },
  },
  hidePostalCode: true
});
card.mount('#card-element');
/*
 *
 *
 *
 *
 action form click
 *
 *
 *
 *
 */
function setOutcome(result) {
  var successElement = document.querySelector('.success');
  var errorElement = document.querySelector('.error');
  successElement.classList.remove('visible');
  errorElement.classList.remove('visible');

  if (result.token) 
  {
    // Use the token to create a charge or a customer
    successElement.querySelector('.token').textContent = result.token.id;
    successElement.classList.add('visible');

      /*
       *
       *
       *
       *
       Pass tokens using ajax to php file
       *
       *
       *
       *
       */
      console.log("came here ?");
      console.log(user_email);
      console.log(user_name);
      console.log(user_tel);

      var data = {'request' : result.token.id,
                  'email_req' : user_email,
                  'name_req' : user_name,
                  'tel_req' : user_tel
                 };

      var request = $.ajax({
        url: "web.php",
        type: "POST",
        data: data,
      });

      request.done(function(msg) {
        alert( "success" + msg );
      });

      request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus + jqXHR);
      });


/*
 *
 *
 *
 *
 ifの終わり
 *
 *
 *
 *
 */
  } else if (result.error) {
    errorElement.textContent = result.error.message;
    errorElement.classList.add('visible');
  }
}

card.on('change', function(event) {
  setOutcome(event);
});
/*
 *
 *
 *
 *
 form buttom click event
 *
 *
 *
 *
 */
document.querySelector('form').addEventListener('submit', function(e) {

      e.preventDefault();

      stripe.createToken(card).then(setOutcome);

      user_email = $('#user_info [name=email]').val();
      user_name = $('#user_info [name=name]').val();
      user_tel = $('#user_info [name=tel]').val();

});
